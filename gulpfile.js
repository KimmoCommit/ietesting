var gulp = require('gulp'),
sass = require('gulp-ruby-sass'),
connect = require('gulp-connect');


gulp.task('sass', function(){
	return gulp.src('sass/main.scss')
	.pipe(sass())
	.pipe(gulp.dest('css/'))
	.pipe(connect.reload());
});


gulp.task('connect', function(){
	connect.server({
		root: '',
		livereload: true,
		port: 3000
	});
});

gulp.task('watch', function(){
 gulp.watch('sass/**/*.scss',['sass']);
});




gulp.task('default',['sass','connect','watch']);